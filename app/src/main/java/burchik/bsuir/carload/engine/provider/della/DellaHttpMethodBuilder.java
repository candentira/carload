package burchik.bsuir.carload.engine.provider.della;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.jsoup.Connection;

import java.util.ArrayList;
import java.util.List;

import burchik.bsuir.carload.engine.data.request.LoginRequestData;
import burchik.bsuir.carload.engine.data.request.LogoutRequestData;
import burchik.bsuir.carload.engine.data.request.SearchRequestData;
import burchik.bsuir.carload.engine.data.request.della.DellaLogoutRequestData;
import burchik.bsuir.carload.engine.provider.CargoHttpMethodBuilder;

public class DellaHttpMethodBuilder implements CargoHttpMethodBuilder {

    private static final Logger LOG = Logger.getLogger(DellaHttpMethodBuilder.class);

    private static final String BASE_HOST = "www.della.by";
    private static final String BASE_URL = "http://" + BASE_HOST;
    private static final String BASE_SEARCH_URL = BASE_URL + "/search/";

    private static final String LOGIN_MODE_ENTER = "enter";
    private static final String LOGIN_MODE_EXIT = "exit";

    private static final String PARAM_LOCATION_URL = "location_url";
    private static final String PARAM_LOGIN_MODE = "login_mode";
    private static final String PARAM_LOGIN = "login";
    private static final String PARAM_PASSWORD = "password";
    private static final String PARAM_COOKIE_ID = "cookie_id";

    @Override
    public <S extends SearchRequestData> Connection getSearchMethod(Connection connection, S searchData) {
        return connection.url(constructSearchUrl(searchData))
                .method(Connection.Method.GET);
    }

    private String constructSearchUrl(SearchRequestData data) {
        String url = new StringBuilder(BASE_SEARCH_URL)
                .append("a").append(StringUtils.join(getCountries(data.getAddressFromList()), "l"))
                .append("b").append(StringUtils.join(getAreas(data.getAddressFromList()), "l"))
                .append("j").append(StringUtils.join(getCities(data.getAddressFromList()), "l"))
                .append("d").append(StringUtils.join(getCountries(data.getAddressToList()), "l"))
                .append("e").append(StringUtils.join(getAreas(data.getAddressToList()), "l"))
                .append("t").append(StringUtils.join(getCities(data.getAddressToList()), "l"))
                .append("f").append(data.getVolumeFrom()).append("l").append(data.getVolumeTo())
                .append("o").append(data.getWeightFrom()).append("l").append(data.getWeightTo())
                .append("h").append(StringUtils.join(data.getTransportTypes(), "l"))
                .append("i").append(data.getDateFrom()).append("l").append(data.getDateTo())
                .append("k0m1")
                .append("r").append(data.getPage() * DellaCargoProvider.RECORDS_PER_PAGE).append("l").append(DellaCargoProvider.RECORDS_PER_PAGE)
                .append(".html")
                .toString();

        return url;
    }

    private List<String> getCountries(List<SearchRequestData.Address> addresses) {
        List<String> countries = new ArrayList<>();
        for (SearchRequestData.Address a : addresses) {
            countries.add(a.getCountry());
        }
        return countries;
    }

    private List<String> getAreas(List<SearchRequestData.Address> addresses) {
        List<String> areas = new ArrayList<>();
        for (SearchRequestData.Address a : addresses) {
            areas.add(a.getArea());
        }
        return areas;
    }

    private List<String> getCities(List<SearchRequestData.Address> addresses) {
        List<String> cities = new ArrayList<>();
        for (SearchRequestData.Address a : addresses) {
            cities.add(a.getCity());
        }
        return cities;
    }

    @Override
    public <L extends LoginRequestData> Connection getLoginMethod(Connection connection, L loginData) {
        return connection.url(BASE_URL)
                .data(PARAM_LOGIN_MODE, LOGIN_MODE_ENTER)
                .data(PARAM_LOCATION_URL, BASE_HOST)
                .data(PARAM_LOGIN, loginData.getLogin())
                .data(PARAM_PASSWORD, loginData.getPassword())
                .method(Connection.Method.POST);
    }

    @Override
    public <L extends LogoutRequestData> Connection getLogoutMethod(Connection connection, L logoutData) {
        DellaLogoutRequestData data = (DellaLogoutRequestData) logoutData;

        return connection.url(BASE_URL)
                .data(PARAM_LOGIN_MODE, LOGIN_MODE_EXIT)
                .data(PARAM_LOGIN, data.getLogin())
                .data(PARAM_COOKIE_ID, data.getCookieId())
                .method(Connection.Method.GET);
    }

    @Override
    public Connection getCheckLoggedMethod(Connection connection) {
        return connection.url(BASE_URL);
    }
}
