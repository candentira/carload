package burchik.bsuir.carload.engine.provider;

import org.jsoup.Connection;

import burchik.bsuir.carload.engine.data.request.LoginRequestData;
import burchik.bsuir.carload.engine.data.request.LogoutRequestData;
import burchik.bsuir.carload.engine.data.request.SearchRequestData;

public interface CargoHttpMethodBuilder {

    <S extends SearchRequestData> Connection getSearchMethod(Connection method, S searchData);

    <L extends LoginRequestData> Connection getLoginMethod(Connection method, L loginData);

    <L extends LogoutRequestData> Connection getLogoutMethod(Connection method, L logoutData);

    Connection getCheckLoggedMethod(Connection connection);
}
