package burchik.bsuir.carload.bean;

import android.os.Parcel;
import android.os.Parcelable;

public class PreferenceAddress implements Parcelable{
    private CountryCode country;
    private AreaCode area;
    private CityCode city;

    public PreferenceAddress(CountryCode country, AreaCode area, CityCode city) {
        this.country = country;
        this.area = area;
        this.city = city;
    }

    public PreferenceAddress(CountryCode country) {
        this(country, AreaCode.ALL, CityCode.ALL);
    }

    public PreferenceAddress(AreaCode area) {
        this(area.getCountry(), area, CityCode.ALL);
    }

    public PreferenceAddress(CityCode city) {
        this(city.getArea().getCountry(), city.getArea(), city);
    }

    private PreferenceAddress(Parcel in) {
        country = in.readParcelable(CountryCode.class.getClassLoader());
        area = in.readParcelable(AreaCode.class.getClassLoader());
        city = in.readParcelable(CityCode.class.getClassLoader());
    }

    public CountryCode getCountry() {
        return country;
    }

    public AreaCode getArea() {
        return area;
    }

    public CityCode getCity() {
        return city;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(country, flags);
        dest.writeParcelable(area, flags);
        dest.writeParcelable(city, flags);
    }

    public static final Parcelable.Creator<PreferenceAddress> CREATOR
            = new Parcelable.Creator<PreferenceAddress>() {
        public PreferenceAddress createFromParcel(Parcel in) {
            return new PreferenceAddress(in);
        }

        public PreferenceAddress[] newArray(int size) {
            return new PreferenceAddress[size];
        }
    };

    @Override
    public String toString() {
        return "PreferenceAddress{" +
                "country=" + country +
                ", area=" + area +
                ", city=" + city +
                '}';
    }
}
