package burchik.bsuir.carload.bean;

import android.os.Parcel;
import android.os.Parcelable;

public class CargoAddress implements Parcelable {
    private String country;
    private String city;

    public CargoAddress(String country, String city) {
        this.country = country;
        this.city = city;
    }

    private CargoAddress(Parcel in) {
        country = in.readString();
        city = in.readString();
    }

    public String getCountry() {
        return country;
    }


    public String getCity() {
        return city;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(country);
        dest.writeString(city);
    }

    public static final Parcelable.Creator<CargoAddress> CREATOR
            = new Parcelable.Creator<CargoAddress>() {
        public CargoAddress createFromParcel(Parcel in) {
            return new CargoAddress(in);
        }

        public CargoAddress[] newArray(int size) {
            return new CargoAddress[size];
        }
    };

    @Override
    public String toString() {
        return "CargoAddress{" +
                "country='" + country + '\'' +
                ", city='" + city + '\'' +
                '}';
    }
}
