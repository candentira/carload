package burchik.bsuir.carload.engine.provider.della;

import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import burchik.bsuir.carload.bean.CarLoad;
import burchik.bsuir.carload.bean.CityCode;
import burchik.bsuir.carload.bean.CountryCode;
import burchik.bsuir.carload.bean.LoginData;
import burchik.bsuir.carload.bean.LogoutData;
import burchik.bsuir.carload.bean.PreferenceAddress;
import burchik.bsuir.carload.bean.SearchData;
import burchik.bsuir.carload.engine.data.response.Response;
import burchik.bsuir.carload.engine.provider.CargoProvider;

public class Test {

    public static void main(String[] args) {
        CargoProvider provider = DellaCargoProvider.INSTANCE;

        try {
            System.out.println();
            Response<?> loginRespose = provider.login(new LoginData("klmnk", "819041875867"));
            System.out.println("login status: " + loginRespose.getStatus());
            System.out.println("login status msg: " + loginRespose.getStatusMessage());

            SearchData data = new SearchData();
            data.setAddressesFrom(Arrays.asList(new PreferenceAddress(CityCode.GOMEL), new PreferenceAddress(CityCode.SVETLOGORSK)));
            data.setAddressesTo(Arrays.asList(new PreferenceAddress(CountryCode.ALL)));
            data.setDateFrom(new Date());
            data.setVolumeFrom(0);
            data.setVolumeTo(100);
            data.setWeightFrom(0);
            data.setWeightTo(25);
            Calendar c = Calendar.getInstance();
            c.add(Calendar.MONTH, 2);
            data.setDateTo(c.getTime());
            data.setLimit(50);
            Response<List<CarLoad>> response = provider.search(data);
            if (response.getStatus() != Response.Status.OK) {
                System.out.println("response.getStatusMessage() = " + response.getStatusMessage());
            } else {
                int i = 1;
                for (CarLoad cl : response.getResult()) {
                    System.out.println("cl.toString() = " + cl.toString());
//                    System.out.println("cl.getId() = " + cl.getId() + ";\tcl.getPhoneString() = " + cl.getContactPhoneString() + "\tcl.getPhonePicture() = " + cl.getContactPhonePicture());
                }
            }
        } finally {
            System.out.println();
            Response<?> logoutRespose = provider.logout(new LogoutData("klmnk"));
            System.out.println("logout status: " + logoutRespose.getStatus());
            System.out.println("logout status msg: " + logoutRespose.getStatusMessage());
        }
    }

    public static void main2(String[] args) {
        CargoProvider provider = DellaCargoProvider.INSTANCE;

        System.out.println();
        Response<?> loginRespose = provider.login(new LoginData("100", "1004"));
        System.out.println("login status: " + loginRespose.getStatus());
        System.out.println("login status msg: " + loginRespose.getStatusMessage());

        System.out.println();
        Response<?> logoutRespose = provider.logout(new LogoutData("100"));
        System.out.println("logout status: " + logoutRespose.getStatus());
        System.out.println("logout status msg: " + logoutRespose.getStatusMessage());
    }
}
