package burchik.bsuir.carload.engine.provider.lardi;

import burchik.bsuir.carload.bean.LoginData;
import burchik.bsuir.carload.bean.LogoutData;
import burchik.bsuir.carload.bean.SearchData;
import burchik.bsuir.carload.engine.data.request.LoginRequestData;
import burchik.bsuir.carload.engine.data.request.LogoutRequestData;
import burchik.bsuir.carload.engine.data.request.SearchRequestData;
import burchik.bsuir.carload.engine.provider.InputDataConverter;

/**
 * Created by aklotos on 23.06.2015.
 */
public class LardiInputDataConverter implements InputDataConverter {
    @Override
    public <S extends SearchData> SearchRequestData requestData(S data) {
        return null;
    }

    @Override
    public <L extends LogoutData> LogoutRequestData requestData(L data) {
        return null;
    }

    @Override
    public <L extends LoginData> LoginRequestData requestData(L data) {
        return null;
    }
}
