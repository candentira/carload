package burchik.bsuir.carload.engine.provider;

/**
 * Created by aklotos on 20.06.2015.
 */
public enum CargoProviderType {
    DELLA, LARDI;
}
