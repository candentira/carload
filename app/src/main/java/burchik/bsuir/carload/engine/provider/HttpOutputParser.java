package burchik.bsuir.carload.engine.provider;

public interface HttpOutputParser<I, O> {
    O parse(I input);
}
