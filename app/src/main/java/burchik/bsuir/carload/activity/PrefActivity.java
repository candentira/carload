package burchik.bsuir.carload.activity;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;
import android.view.MenuItem;
import android.view.Window;
import android.widget.Toast;

import java.util.List;

import burchik.bsuir.carload.R;
import burchik.bsuir.carload.activity.fragment.CarLoadNotificationPreferenceFragment;
import burchik.bsuir.carload.activity.fragment.CarLoadPreferenceFragment;
import burchik.bsuir.carload.activity.fragment.CarLoadSearchPreferenceFragment;
import burchik.bsuir.carload.activity.preference.SeekBarPreference;

public class PrefActivity extends PreferenceActivity {

    @Override
    public void onBuildHeaders(List<Header> target) {
        loadHeadersFromResource(R.xml.pref_headers, target);
    }

    @Override
    protected boolean isValidFragment(String fragmentName) {
        return CarLoadPreferenceFragment.class.getName().equals(fragmentName) ||
                CarLoadSearchPreferenceFragment.class.getName().equals(fragmentName) || CarLoadNotificationPreferenceFragment.class.getName().equals(fragmentName);
    }

    /*@Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(android.R.id.home == item.getItemId()){
            finish();
        }
        return super.onOptionsItemSelected(item);
    }*/
}
