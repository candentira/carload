package burchik.bsuir.carload.bean;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import java.io.UnsupportedEncodingException;

import burchik.bsuir.carload.R;

/**
 * Created by Candentira on 24.05.2015.
 */
public class Size implements Parcelable {
    private static final String TAG = "carLoad.Size";

    private int id;
    private Double height;
    private Double length;
    private Double width;
    private Double volume;

    public Size(double height, double length, double width) {
        this.height = height;
        this.length = length;
        this.width = width;
        volume = height * length * width;
    }

    public Size(Double volume) {
        this.volume = volume;
    }

    public Double getVolume() {
        return volume;
    }

    public void setVolume(double volume) {
        this.volume = volume;
    }

    public Double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public Double getLength() {
        return length;
    }

    public void setLength(double length) {
        this.length = length;
    }

    public Double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    @Override
    public String toString() {
        String sizeString = "";
            if (height != null && length != null && width != null) {
                sizeString = height + "*" + width + "*" + length + " (в/ш/д)";
            } else {
                sizeString = volume + " м3";
            }
        return sizeString;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeDouble(volume);
        try{
            dest.writeDouble(height);
            dest.writeDouble(length);
            dest.writeDouble(width);
        }catch (Exception e){
            Log.v(TAG, "in Exception");
        }
    }

    public static final Parcelable.Creator<Size> CREATOR
            = new Parcelable.Creator<Size>() {
        public Size createFromParcel(Parcel in) {
            return new Size(in);
        }

        public Size[] newArray(int size) {
            return new Size[size];
        }
    };

    private Size(Parcel in) {
        volume = in.readDouble();
        try{
            height = in.readDouble();
            length = in.readDouble();
            width = in.readDouble();
        }catch (Exception e){
            Log.v(TAG, "in Exception");
        }
    }
}
