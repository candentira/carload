package burchik.bsuir.carload.engine.provider.della.parser;

import org.apache.commons.lang3.StringUtils;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;
import java.util.List;

import burchik.bsuir.carload.bean.CargoAddress;
import burchik.bsuir.carload.bean.PreferenceAddress;
import burchik.bsuir.carload.bean.CarLoad;
import burchik.bsuir.carload.bean.CountryCode;
import burchik.bsuir.carload.engine.provider.HttpOutputParser;

public class DellaCarLoadParser implements HttpOutputParser<Document, List<CarLoad>> {

    @Override
    public List<CarLoad> parse(Document document) {
        List<CarLoad> carLoadList = new ArrayList<>();
        Elements elements = document.select(".request_level_ms");

        for (Element element : elements) {
            String id = element.attr("request_id");
            if (StringUtils.isEmpty(id)) {
                continue;
            }

            try {
                String date = element.select("td.multi_date > div > b").text();
                String[] fromTo = element.select(".request_distance").html().split(" — ");
                List<CargoAddress> fromAddresses = getAddresses(fromTo[0]);
                List<CargoAddress> toAddresses = getAddresses(fromTo[1]);
                String price = element.select(".m_comment.pr_10").text();
                String type = element.select(".truck").text();
                String weight = element.select(".weight").text();
                String size = element.select(".cube").text();
                String description = element.select(".m_text_gray.m_txt_gr").text();
                String distance = element.select("a.distance_link").text();
                String[] changedCreated = element.select("span.izm_razm").html().replaceAll("&nbsp;", " ").split("<br>");
                String changed = changedCreated[0].replace("изм. ", "");
                String created = changedCreated[1].replace("разм. ", "");
                String link = element.select("a.request_distance").attr("href");
                String contactName = element.select("div.fl_l.m_blue_phone:not(.nobr)").text();
                String phoneString = element.select("div.fl_l.m_blue_phone.nobr > a").text();
                byte[] phonePicture = new byte[0];
                String phoneLink = element.select("div.fl_l.m_blue_phone.nobr > img").attr("src");
                if (!StringUtils.isEmpty(phoneLink)) {
                    phonePicture = Jsoup.connect("http://www.della.by" + phoneLink)
                            .ignoreContentType(true)
                            .method(Connection.Method.GET)
                            .execute()
                            .bodyAsBytes();
                }
                String contactSkype = element.select("a.skyp").text();
                String contactEmail = element.select("a[email_num]").text();

                CarLoad carLoad = new CarLoad();
                carLoad.setId(id);
                carLoad.setDate(date);
                carLoad.setFrom(fromAddresses);
                carLoad.setTo(toAddresses);
                carLoad.setPrice(price);
                carLoad.setType(type);
                carLoad.setWeight(weight);
                carLoad.setSize(size);
                carLoad.setDescription(description);
                carLoad.setDistance(distance);
                carLoad.setChanged(changed);
                carLoad.setCreated(created);
                carLoad.setLink(link);
                carLoad.setContactPhoneString(phoneString);
                carLoad.setContactPhonePicture(phonePicture);
                carLoad.setContactSkype(contactSkype);
                carLoad.setContactEmail(contactEmail);
                carLoad.setContactName(contactName);
                carLoadList.add(carLoad);
            } catch (Throwable e) {
                e.printStackTrace();
                continue;
            }
        }

        return carLoadList;
    }

    private List<CargoAddress> getAddresses(String totalFrom) {
        List<CargoAddress> addresses = new ArrayList<>();
        for (String from : totalFrom.split("</span>")) {
            if (!StringUtils.isBlank(from)) {
                addresses.add(getAddress(from));
            }
        }
        return addresses;
    }

    private CargoAddress getAddress(String from) {
        String fromCountry = from.replaceFirst(".*\\((.{2})\\).*", "$1");
        String fromCity = from.replaceFirst(".*<b>(.*)</b>.*", "$1");
        return new CargoAddress(fromCountry, fromCity);
    }
}
