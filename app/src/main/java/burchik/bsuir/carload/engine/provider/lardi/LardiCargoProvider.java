package burchik.bsuir.carload.engine.provider.lardi;

import org.jsoup.Connection;
import org.jsoup.nodes.Document;

import java.util.List;

import burchik.bsuir.carload.bean.CarLoad;
import burchik.bsuir.carload.bean.LoginData;
import burchik.bsuir.carload.bean.LogoutData;
import burchik.bsuir.carload.bean.SearchData;
import burchik.bsuir.carload.engine.data.response.Response;
import burchik.bsuir.carload.engine.provider.CargoHttpMethodBuilder;
import burchik.bsuir.carload.engine.provider.CargoProvider;
import burchik.bsuir.carload.engine.provider.HttpOutputParser;
import burchik.bsuir.carload.engine.provider.InputDataConverter;
import burchik.bsuir.carload.engine.provider.lardi.parser.LardiCarLoadParser;
import burchik.bsuir.carload.engine.provider.lardi.parser.LardiLoginParser;

/**
 * Created by aklotos on 23.06.2015.
 */
public final class LardiCargoProvider extends CargoProvider {
    
    private InputDataConverter converter = new LardiInputDataConverter();
    private CargoHttpMethodBuilder builder = new LardiHttpMethodBuilder();
    private HttpOutputParser<Document, List<CarLoad>> carLoadParser = new LardiCarLoadParser();
    private HttpOutputParser<Connection.Response, Response<String>> loginParser = new LardiLoginParser();
    
    @Override
    public Response<?> logout(LogoutData logoutData) {
        return null;
    }

    @Override
    public Response<?> login(LoginData loginData) {
        return null;
    }

    @Override
    public Response<List<CarLoad>> search(SearchData searchData) {
        return null;
    }
}
