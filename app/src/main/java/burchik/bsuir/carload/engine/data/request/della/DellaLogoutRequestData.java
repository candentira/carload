package burchik.bsuir.carload.engine.data.request.della;

import burchik.bsuir.carload.engine.data.request.LogoutRequestData;

/**
 * Created by aklotos on 20.06.2015.
 */
public class DellaLogoutRequestData extends LogoutRequestData {
    private String login;
    private String cookieId;

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getCookieId() {
        return cookieId;
    }

    public void setCookieId(String cookieId) {
        this.cookieId = cookieId;
    }

    @Override
    public String toString() {
        return "DellaLogoutRequestData{" +
                "login='" + login + '\'' +
                ", cookieId='" + cookieId + '\'' +
                '}';
    }
}
