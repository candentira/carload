package burchik.bsuir.carload.activity;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.os.Parcelable;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ExecutionException;

import burchik.bsuir.carload.R;
import burchik.bsuir.carload.bean.CarLoad;
import burchik.bsuir.carload.bean.SearchData;
import burchik.bsuir.carload.data.DataLoader;
import burchik.bsuir.carload.data.DatabaseHelper;

public class UpdateNotificationService extends Service {
    private static final String TAG = "carLoad.Service";

    private static final int defaultReloadDelay = 3;

    NotificationManager notificationManager;
    TimerTask task;
    private static DatabaseHelper dbHepler;
    private int mId = 0;

    public UpdateNotificationService() {
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.v(TAG, "in onCreate");
        initializeDatabaseHelper();
        notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        //notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
    }

    public void initializeDatabaseHelper(){
        if(dbHepler == null){
            dbHepler = DatabaseHelper.getInstance(getApplicationContext());
        }
    }

    @Override
    public void onDestroy() {
        Log.v(TAG, "in onDestroy");
        dbHepler.close();
        super.onDestroy();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.v(TAG, "in onStartCommand");
        if(intent != null) {
            boolean isTimer = intent.getBooleanExtra(LoadsListActivity.IS_TIMER, false);
            final int reloadDelay = intent.getIntExtra(LoadsListActivity.RELOAD_DELAY, defaultReloadDelay);
            SearchData searchData = intent.getParcelableExtra(LoadsListActivity.SEARCH_DATA);
            //if (searchUrl != null && !"".equals(searchUrl) && !DataLoader.SEARCH_URL.equals(searchUrl)) {
                //dbHepler.deleteCarLoads();
            //    DataLoader.setSearchUrl(searchUrl);
            //}
            boolean hideWasShown = intent.getBooleanExtra(LoadsListActivity.HIDE_WAS_SHOWN, false);

            if (isTimer) {
                startCyclicCarLoadUpdate(searchData, reloadDelay, hideWasShown);
            } else {
                doCarLoadUpdate(searchData, hideWasShown);
            }
        }
        return super.onStartCommand(intent, flags, startId);
    }

    private void startCyclicCarLoadUpdate(final SearchData searchData, final int reloadDelay, final boolean hideWasShown) {
        Log.v(TAG, "in startCyclicCarLoadUpdate, reloadDelay: " + reloadDelay);
        final Handler handler = new Handler();
        Timer timer = new Timer();
        task = new TimerTask() {
            @Override
            public void run() {
                handler.post(new Runnable() {
                    public void run() {
                        checkForNewCarLoads(searchData,  hideWasShown);
                    }
                });
            }
        };
        timer.schedule(task, 0, 60 * 1000 * reloadDelay);
    }

    private void doCarLoadUpdate(final SearchData searchData, final boolean hideWasShown) {
        Log.v(TAG, "in doCarLoadUpdate");
        new Thread(new Runnable() {
            public void run() {
                checkForNewCarLoads(searchData, hideWasShown);
                stopSelf();
            }
        }).start();
    }

    private void checkForNewCarLoads(SearchData searchData, boolean hideWasShown){
        Log.v(TAG, "in checkForNewCarLoads");
        List<CarLoad> carLoads = updateCarLoadList(searchData, hideWasShown);
        Intent intent = new Intent(LoadsListActivity.BROADCAST_ACTION);
        intent.putParcelableArrayListExtra(LoadsListActivity.CAR_LOAD_LIST, new ArrayList<Parcelable>(carLoads));
        sendBroadcast(intent);
    }

    public List<CarLoad> updateCarLoadList(SearchData searchData, boolean hideWasShown) {
        Log.v(TAG, "in updateCarLoadList");
        List<CarLoad> carLoadList = new ArrayList<>();
        int newCardLoadsAmount = 0;
        try {
            carLoadList = new DataLoader().execute(searchData).get();
            for (int i = 0; i < carLoadList.size(); i++) {

                CarLoad carLoad = carLoadList.get(i);

                System.out.println("**************************");
                System.out.println("carLoad.getContactPhoneString() = " + carLoad.getContactPhoneString());
                System.out.println("**************************");

                CarLoad dbCarLoad = dbHepler.getCarLoad(carLoad.getId());
                if (dbCarLoad == null) {
                    dbHepler.addCarLoad(carLoad);
                    newCardLoadsAmount++;
                } else {
                    carLoad.setWasShown(dbCarLoad.getWasShown());
                    //dbHepler.updateCarLoadShown(carLoad.getId(), true);
                }
            }
            Log.v(TAG, "CarLoad entities: " + dbHepler.getCarLoadsCount());
            Log.v(TAG, "New CarLoads: " + newCardLoadsAmount);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        if(newCardLoadsAmount > 0){
            sendNotification(newCardLoadsAmount);
        }
        return carLoadList;
    }

    private void sendNotification(int newCardLoadsAmount) {
        Intent intent = new Intent(this, LoadsListActivity.class);
        intent.putExtra(LoadsListActivity.NEW_CARLOAD_AMOUNT, newCardLoadsAmount);
        PendingIntent pIntent = PendingIntent.getActivity(this, 0, intent, 0);

        Notification notif = new Notification.Builder(this)
                .setContentTitle(getText(R.string.new_load_notif_title))
                .setContentText(getText(R.string.new_load_notif_text))
                .setSmallIcon(R.mipmap.truck)
                .setContentIntent(pIntent)
                .getNotification();

        notif.flags |= Notification.FLAG_AUTO_CANCEL;

        notificationManager.notify(1, notif);
    }

    @Override
    public IBinder onBind(Intent intent) {
        Log.v(TAG, "in onBind");
        // TODO: Return the communication channel to the service.
        //throw new UnsupportedOperationException("Not yet implemented");
        return null;
    }
}
