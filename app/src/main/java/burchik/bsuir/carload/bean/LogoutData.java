package burchik.bsuir.carload.bean;

public class LogoutData {
    private String login;

    public LogoutData(String login) {
        this.login = login;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    @Override
    public String toString() {
        return "LogoutData{" +
                "login='" + login + '\'' +
                '}';
    }
}
