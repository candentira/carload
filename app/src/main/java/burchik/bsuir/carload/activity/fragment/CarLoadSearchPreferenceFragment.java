package burchik.bsuir.carload.activity.fragment;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.ListPreference;
import android.preference.PreferenceFragment;

import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import burchik.bsuir.carload.R;
import burchik.bsuir.carload.activity.preference.DatePreference;
import burchik.bsuir.carload.activity.preference.SeekBarPreference;
import burchik.bsuir.carload.bean.AreaCode;
import burchik.bsuir.carload.bean.CountryCode;

public class CarLoadSearchPreferenceFragment extends PreferenceFragment implements SharedPreferences.OnSharedPreferenceChangeListener {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.pref_search);
    }

    @Override
    public void onResume() {
        super.onResume();
        getPreferenceScreen().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        getPreferenceScreen().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        //dbHepler.deleteCarLoads();
        switch (key) {
            case "from_country_key":
                int country = sharedPreferences.getInt(getString(R.string.from_country_key), CountryCode.ALL.ordinal());
                populateRegion(country);
                break;
            case "till_time_key":
                Date fromTime = null;
                Date toTime = null;
                reload();
                try {
                    fromTime = DatePreference.formatter().parse(sharedPreferences.getString(getString(R.string.from_time_key), Calendar.getInstance().toString()));
                    toTime = DatePreference.formatter().parse(sharedPreferences.getString(getString(R.string.till_time_key), Calendar.getInstance().toString()));
                    if (fromTime != null && toTime != null &&fromTime.after(toTime)) {
                        SharedPreferences.Editor prefEditor = sharedPreferences.edit();
                        prefEditor.putString(getString(R.string.till_time_key), DatePreference.formatter().format(new Date(fromTime.getTime() + 1000 * 60 * 60 * 24)));
                        prefEditor.commit();
                        //showErrorDialog("Дата окончания должна быть больше даты старта!");
                    }
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                break;
            case "from_time_key":
                reload();
                try {
                    fromTime = DatePreference.formatter().parse(sharedPreferences.getString(getString(R.string.from_time_key), Calendar.getInstance().toString()));
                    toTime = DatePreference.formatter().parse(sharedPreferences.getString(getString(R.string.till_time_key), Calendar.getInstance().toString()));
                    if (fromTime != null && toTime != null &&fromTime.after(toTime)) {
                        SharedPreferences.Editor prefEditor = sharedPreferences.edit();
                        prefEditor.putString(getString(R.string.from_time_key), DatePreference.formatter().format(new Date(toTime.getTime() - 1000 * 60 * 60 * 24)));
                        prefEditor.commit();
                        //showErrorDialog("Дата старта должна быть меньше даты окончания!");
                    }
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                break;
            case "to_weight_key":
                int fromWeight = sharedPreferences.getInt(getString(R.string.from_weight_key), SeekBarPreference.DEFAULT_MIN_VALUE);
                int toWeight = sharedPreferences.getInt(getString(R.string.to_weight_key), SeekBarPreference.DEFAULT_MAX_VALUE);
                if (fromWeight > toWeight) {
                    SharedPreferences.Editor prefEditor = sharedPreferences.edit();
                    prefEditor.putInt(getString(R.string.to_weight_key), fromWeight + 1);
                    prefEditor.commit();
                    //showErrorDialog("Максимальный вес должен быть больше минимального!");
                }
                break;
            case "from_weight_key":
                fromWeight = sharedPreferences.getInt(getString(R.string.from_weight_key), SeekBarPreference.DEFAULT_MIN_VALUE);
                toWeight = sharedPreferences.getInt(getString(R.string.to_weight_key), SeekBarPreference.DEFAULT_MAX_VALUE);
                if (fromWeight > toWeight) {
                    SharedPreferences.Editor prefEditor = sharedPreferences.edit();
                    prefEditor.putInt(getString(R.string.from_weight_key), toWeight - 1);
                    prefEditor.commit();
                    //showErrorDialog("Минимальный вес должен быть меньше максимального!");
                }
                break;
            case "to_volume_key":
                int fromVolume = sharedPreferences.getInt(getString(R.string.from_volume_key), SeekBarPreference.DEFAULT_MIN_VALUE);
                int toVolume = sharedPreferences.getInt(getString(R.string.to_volume_key), SeekBarPreference.DEFAULT_MAX_VALUE);
                if (fromVolume > toVolume) {
                    SharedPreferences.Editor prefEditor = sharedPreferences.edit();
                    prefEditor.putInt(getString(R.string.to_volume_key), fromVolume + 1);
                    prefEditor.commit();
                    //showErrorDialog("Максимальный объем должен быть больше минимального!");
                }
                break;
            case "from_volume_key":
                fromVolume = sharedPreferences.getInt(getString(R.string.from_volume_key), SeekBarPreference.DEFAULT_MIN_VALUE);
                toVolume = sharedPreferences.getInt(getString(R.string.to_volume_key), SeekBarPreference.DEFAULT_MAX_VALUE);
                if (fromVolume > toVolume) {
                    SharedPreferences.Editor prefEditor = sharedPreferences.edit();
                    prefEditor.putInt(getString(R.string.from_volume_key), toVolume - 1);
                    prefEditor.commit();
                    //showErrorDialog("Минимальный объем должен быть меньше максимального!");
                }
                break;
            default:
                break;
        }
    }

    private void populateRegion(int country) {
        ListPreference listPreferenceCategory = (ListPreference) findPreference("from_region_key");
        if (listPreferenceCategory != null) {
            List<AreaCode> areaList = AreaCode.getByCountry(CountryCode.fromOrdinal(country));
            CharSequence entries[] = new String[areaList.size()];
            CharSequence entryValues[] = new String[areaList.size()];
            int i = 0;
            for (AreaCode areaCode : areaList) {
                entries[i] = areaCode.getName();
                entryValues[i] = areaCode.getDellaCode();
                i++;
            }
            listPreferenceCategory.setEntries(entries);
            listPreferenceCategory.setEntryValues(entryValues);
        }
    }

    private void showErrorDialog(String errorString) {
        Context context = getActivity();
        AlertDialog.Builder ad = new AlertDialog.Builder(context);
        ad.setTitle(context.getString(R.string.error_name));
        ad.setMessage(errorString);
        ad.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                reload();
            }
        });
        ad.show();
    }

    private void reload() {
        startActivity(getActivity().getIntent());
        getActivity().finish();
    }
}
