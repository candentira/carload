package burchik.bsuir.carload.bean;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

public enum AreaCode implements Parcelable {
    ALL(CountryCode.ALL, "Все", "0"),

    // BY
    BRESTSKAJA(CountryCode.BY, "Брестская", "143"),
    VITEBSKAJA(CountryCode.BY, "Витебская", "29"),
    GOMELSKAJA(CountryCode.BY, "Гомельская", "30"),
    GRODNENSKAJA(CountryCode.BY, "Гродненская", "121"),
    MINSKAJA(CountryCode.BY, "Минская", "142"),
    MOGILEVSKAJA(CountryCode.BY, "Могилевская", "144"),

    // RU
    BRYANSKAJA(CountryCode.RU, "Брянская", "42"),
    KALUZHSKAYA(CountryCode.RU, "Калужская", "56"),
    MOSKOVSKAJA(CountryCode.RU, "Московская", "75"),
    ORLOVSKAJA(CountryCode.RU, "Орловская", "83"),
    SMOLENSKAJA(CountryCode.RU, "Смоленская", "96"),
    TVERSKAJA(CountryCode.RU, "Тверская", "101"),
    TULSKAJA(CountryCode.RU, "Тульская", "104");

    private CountryCode country;
    private String name;
    private String dellaCode;

    AreaCode(CountryCode country, String area, String dellaCode) {
        this.country = country;
        this.name = area;
        this.dellaCode = dellaCode;
    }

    public CountryCode getCountry() {
        return country;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    private void setCountry(CountryCode country) {
        this.country = country;
    }

    public String getDellaCode() {
        return dellaCode;
    }

    private void setDellaCode(String dellaCode) {
        this.dellaCode = dellaCode;
    }

    public static List<AreaCode> getByCountry(CountryCode country) {
        List<AreaCode> areas = new ArrayList<>();
        areas.add(ALL);
        for (AreaCode area : AreaCode.values()) {
            if (area.getCountry() == country) {
                areas.add(area);
            }
        }
        return areas;
    }

    public static AreaCode fromOrdinal(int ordinal) {
        return AreaCode.values()[ordinal];
    }

    public static final Parcelable.Creator<AreaCode> CREATOR = new Parcelable.Creator<AreaCode>() {
        public AreaCode createFromParcel(Parcel in) {
            AreaCode area = AreaCode.values()[in.readInt()];
            area.setCountry(CountryCode.values()[in.readInt()]);
            area.setName(in.readString());
            area.setDellaCode(in.readString());
            return area;
        }

        public AreaCode[] newArray(int size) {
            return new AreaCode[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel out, int flags) {
        out.writeInt(ordinal());
        out.writeInt(country.ordinal());
        out.writeString(name);
        out.writeString(dellaCode);
    }
}
