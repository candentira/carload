package burchik.bsuir.carload.engine.provider.lardi;

import org.jsoup.Connection;

import burchik.bsuir.carload.engine.data.request.LoginRequestData;
import burchik.bsuir.carload.engine.data.request.LogoutRequestData;
import burchik.bsuir.carload.engine.data.request.SearchRequestData;
import burchik.bsuir.carload.engine.provider.CargoHttpMethodBuilder;

public class LardiHttpMethodBuilder implements CargoHttpMethodBuilder {
    @Override
    public Connection getCheckLoggedMethod(Connection connection) {
        return null;
    }

    @Override
    public <L extends LogoutRequestData> Connection getLogoutMethod(Connection method, L logoutData) {
        return null;
    }

    @Override
    public <L extends LoginRequestData> Connection getLoginMethod(Connection method, L loginData) {
        return null;
    }

    @Override
    public <S extends SearchRequestData> Connection getSearchMethod(Connection method, S searchData) {
        return null;
    }
}
