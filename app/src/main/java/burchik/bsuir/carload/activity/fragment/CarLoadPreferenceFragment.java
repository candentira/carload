package burchik.bsuir.carload.activity.fragment;

import android.os.Bundle;
import android.preference.PreferenceFragment;

import burchik.bsuir.carload.R;

public class CarLoadPreferenceFragment extends PreferenceFragment {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.pref_app);
    }

}
