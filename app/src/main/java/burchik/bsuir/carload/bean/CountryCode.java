package burchik.bsuir.carload.bean;

import android.os.Parcel;
import android.os.Parcelable;

public enum CountryCode implements Parcelable {
    ALL("0"),
    AT("130"),
    AZ("1"),
    AL("2"),
    DZ("3"),
    AD("5"),
    AM("10"),
    AF("1"),
    BD("219"),
    BY("16"),
    BE("17"),
    BG("28"),
    BA("23"),
    GB("206"),
    VA("83"),
    HU("86"),
    VN("212"),
    DE("71"),
    NL("138"),
    HK("85"),
    GR("74"),
    GE("70"),
    DK("50"),
    EG("56"),
    IL("93"),
    IN("88"),
    ID("89"),
    JO("97"),
    IQ("91"),
    IR("90"),
    IE("92"),
    ES("221"),
    IT("94"),
    KZ("98"),
    CN("38"),
    KP("101"),
    KG("104"),
    LA("105"),
    LV("106"),
    LB("107"),
    LY("110"),
    LT("112"),
    LI("111"),
    LU("113"),
    MK("115"),
    MT("121"),
    MA("133"),
    MD("129"),
    MC("130"),
    MN("131"),
    NO("148"),
    AE("205"),
    PK("150"),
    PL("158"),
    PT("159"),
    RU("164"),
    RO("163"),
    SA("171"),
    RS("173"),
    SG("176"),
    SY("189"),
    SK("177"),
    SI("178"),
    TJ("191"),
    TH("193"),
    TW("190"),
    TM("200"),
    TR("199"),
    UZ("209"),
    UA("204"),
    FI("65"),
    FR("66"),
    HR("47"),
    ME("225"),
    CZ("50"),
    CH("188"),
    SE("187"),
    LK("182"),
    EE("60"),
    KR("102");

    private String dellaCode;

    CountryCode(String dellaCode) {
        this.dellaCode = dellaCode;
    }

    public String getDellaCode() {
        return dellaCode;
    }

    public void setDellaCode(String dellaCode) {
        this.dellaCode = dellaCode;
    }

    public static final Parcelable.Creator<CountryCode> CREATOR = new Parcelable.Creator<CountryCode>() {

        public CountryCode createFromParcel(Parcel in) {
            CountryCode countryEnum = CountryCode.values()[in.readInt()];
            countryEnum.setDellaCode(in.readString());
            return countryEnum;
        }

        public CountryCode[] newArray(int size) {
            return new CountryCode[size];
        }

    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel out, int flags) {
        out.writeInt(ordinal());
        out.writeString(dellaCode);
    }

    public static CountryCode fromOrdinal(int ordinal) {
        return CountryCode.values()[ordinal];
    }
}
