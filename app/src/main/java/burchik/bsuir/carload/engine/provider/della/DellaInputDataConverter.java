package burchik.bsuir.carload.engine.provider.della;

import org.apache.log4j.Logger;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import burchik.bsuir.carload.bean.LoginData;
import burchik.bsuir.carload.bean.LogoutData;
import burchik.bsuir.carload.bean.PreferenceAddress;
import burchik.bsuir.carload.bean.SearchData;
import burchik.bsuir.carload.bean.TransportTypeEnum;
import burchik.bsuir.carload.engine.data.request.LoginRequestData;
import burchik.bsuir.carload.engine.data.request.LogoutRequestData;
import burchik.bsuir.carload.engine.data.request.SearchRequestData;
import burchik.bsuir.carload.engine.data.request.della.DellaLogoutRequestData;
import burchik.bsuir.carload.engine.provider.CargoProviderType;
import burchik.bsuir.carload.engine.provider.InputDataConverter;

public final class DellaInputDataConverter implements InputDataConverter {

    private static final Logger LOG = Logger.getLogger(DellaInputDataConverter.class);

    private static final SimpleDateFormat SDF = new SimpleDateFormat("yyMMdd");

    @Override
    public <S extends SearchData> SearchRequestData requestData(S data) {
        LOG.info("Start converting to DELLA request data: ");
        LOG.info("INPUT search data: " + data.toString());

        SearchRequestData requestData = new SearchRequestData();
        requestData.setAddressFromList(requestData(data.getAddressesFrom()));
        requestData.setAddressToList(requestData(data.getAddressesTo()));
        requestData.setDateFrom(data.getDateFrom() == null ? "" : SDF.format(data.getDateFrom()));
        requestData.setDateTo(data.getDateFrom() == null ? "" : SDF.format(data.getDateTo()));
        requestData.setVolumeFrom(data.getVolumeFrom() > 0 ? String.valueOf(data.getVolumeFrom()) : "");
        requestData.setVolumeTo(data.getVolumeTo() > 0 ? String.valueOf(data.getVolumeTo()) : "");
        requestData.setWeightFrom(data.getWeightFrom() > 0 ? String.valueOf(data.getWeightFrom()) : "");
        requestData.setWeightTo(data.getWeightTo() > 0 ? String.valueOf(data.getWeightTo()) : "");
        requestData.setTransportTypes(Arrays.asList(data.getTransportType() == null
                ? TransportTypeEnum.LYUBOJ.getCode(CargoProviderType.DELLA)
                : data.getTransportType().getCode(CargoProviderType.DELLA)));
        requestData.setLimit(data.getLimit());

        LOG.info("OUTPUT search data: " + requestData.toString());

        return requestData;
    }

    private List<SearchRequestData.Address> requestData(List<PreferenceAddress> addresses) {
        List<SearchRequestData.Address> fromAddresses = new ArrayList<>();
        for (PreferenceAddress address: addresses) {
            SearchRequestData.Address from = new SearchRequestData.Address();
            from.setCountry(address.getCountry().getDellaCode());
            from.setArea(address.getArea().getDellaCode());
            from.setCity(address.getCity().getDellaCode());
            fromAddresses.add(from);
        }
        return fromAddresses;
    }

    @Override
    public <L extends LoginData> LoginRequestData requestData(L data) {
        LOG.info("Start converting to DELLA request data: ");
        LOG.info("INPUT search data: " + data.toString());

        LoginRequestData requestData = new LoginRequestData();
        requestData.setLogin(data.getLogin());
        requestData.setPassword(data.getPassword());

        LOG.info("OUTPUT search data: " + requestData.toString());

        return requestData;
    }

    @Override
    public <L extends LogoutData> LogoutRequestData requestData(L data) {
        LOG.info("Start converting to DELLA request data: ");
        LOG.info("INPUT search data: " + data.toString());

        DellaLogoutRequestData requestData = new DellaLogoutRequestData();
        requestData.setLogin(data.getLogin());

        LOG.info("OUTPUT search data: " + requestData.toString());

        return requestData;
    }
}
