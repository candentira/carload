package burchik.bsuir.carload.engine.provider;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.log4j.Logger;
import org.apache.log4j.spi.LoggerFactory;
import org.jsoup.Connection;
import org.jsoup.Jsoup;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import burchik.bsuir.carload.bean.CarLoad;
import burchik.bsuir.carload.bean.LoginData;
import burchik.bsuir.carload.bean.LogoutData;
import burchik.bsuir.carload.bean.SearchData;
import burchik.bsuir.carload.engine.data.response.Response;
import burchik.bsuir.carload.engine.data.response.ResponseHelper;

public abstract class CargoProvider {
    private static final Logger LOG = Logger.getLogger(CargoProvider.class);

    protected String HEADER_CONNECTION = "Connection";
    protected String CONNECTION_KEEP_ALIVE = "keep-alive";
    protected String USER_AGENT = "Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.124 Safari/537.36";

    protected Map<String, String> cookies = new HashMap<>();

    public abstract Response<List<CarLoad>> search(SearchData searchData);

    public abstract Response<?> login(LoginData loginData);

    public abstract Response<?> logout(LogoutData logoutData);

    protected Connection newConnection() {
        Connection con = Jsoup.connect("http://google.com")
                .userAgent(USER_AGENT)
                .header(HEADER_CONNECTION, CONNECTION_KEEP_ALIVE);
        if (!MapUtils.isEmpty(cookies)) {
            con.cookies(cookies);
        }
        return con;
    }


    protected Response<Connection.Response> execute(Connection con) {
        Connection.Response response = null;
        try {
            LOG.debug("Executing request: " + con.request().method().toString() + " " + con.request().url().toString());
            response = con.execute();
        } catch (Throwable e) {
            LOG.error("Server error occurred during request execution!", e);
            return ResponseHelper.error("Server error occurred during request execution!\n" + e.getClass().getName() + ": " + e.getMessage());
        }

        processCookies(response.cookies());
        return ResponseHelper.ok(response);
    }

    private void processCookies(Map<String, String> responseCookies) {
        LOG.info("Processing cookies...");

        if (responseCookies != null) {
            LOG.debug("Response cookies: " + Arrays.toString(responseCookies.entrySet().toArray()));
            for (Map.Entry<String, String> entry : responseCookies.entrySet()) {
                cookies.put(entry.getKey(), entry.getValue());
            }
        }
    }

}
