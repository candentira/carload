package burchik.bsuir.carload.engine.provider;

import burchik.bsuir.carload.bean.LoginData;
import burchik.bsuir.carload.bean.LogoutData;
import burchik.bsuir.carload.bean.SearchData;
import burchik.bsuir.carload.engine.data.request.LoginRequestData;
import burchik.bsuir.carload.engine.data.request.LogoutRequestData;
import burchik.bsuir.carload.engine.data.request.SearchRequestData;

public interface InputDataConverter {

    <L extends LoginData> LoginRequestData requestData(L data);

    <L extends LogoutData>LogoutRequestData requestData(L data);

    <S extends SearchData> SearchRequestData requestData(S data);
}
