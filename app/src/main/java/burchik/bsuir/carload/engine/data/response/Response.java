package burchik.bsuir.carload.engine.data.response;

/**
 * Created by aklotos on 21.06.2015.
 */
public class Response<T> {

    public enum Status { OK, ERROR };

    Response(T result, Status status, String statusMessage) {
        this.result = result;
        this.status = status;
        this.statusMessage = statusMessage;
    }

    private T result;
    private Status status;
    private String statusMessage;

    public T getResult() {
        return result;
    }

    public Status getStatus() {
        return status;
    }

    public String getStatusMessage() {
        return statusMessage;
    }
}
