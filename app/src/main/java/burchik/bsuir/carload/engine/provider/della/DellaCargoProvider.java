package burchik.bsuir.carload.engine.provider.della;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.jsoup.Connection;
import org.jsoup.nodes.Document;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import burchik.bsuir.carload.bean.CarLoad;
import burchik.bsuir.carload.bean.LoginData;
import burchik.bsuir.carload.bean.LogoutData;
import burchik.bsuir.carload.bean.SearchData;
import burchik.bsuir.carload.engine.data.request.SearchRequestData;
import burchik.bsuir.carload.engine.data.request.della.DellaLogoutRequestData;
import burchik.bsuir.carload.engine.data.response.Response;
import burchik.bsuir.carload.engine.data.response.ResponseHelper;
import burchik.bsuir.carload.engine.provider.CargoHttpMethodBuilder;
import burchik.bsuir.carload.engine.provider.CargoProvider;
import burchik.bsuir.carload.engine.provider.HttpOutputParser;
import burchik.bsuir.carload.engine.provider.InputDataConverter;
import burchik.bsuir.carload.engine.provider.della.parser.DellaCarLoadParser;
import burchik.bsuir.carload.engine.provider.della.parser.DellaLoginParser;

public final class DellaCargoProvider extends CargoProvider {
    private static final Logger LOG = Logger.getLogger(DellaCargoProvider.class);

    public static final int RECORDS_PER_PAGE = 25;

    private InputDataConverter converter = new DellaInputDataConverter();
    private CargoHttpMethodBuilder builder = new DellaHttpMethodBuilder();
    private HttpOutputParser<Document, List<CarLoad>> carLoadParser = new DellaCarLoadParser();
    private HttpOutputParser<Connection.Response, Response<String>> loginParser = new DellaLoginParser();
    private String cookieId;

    public static final DellaCargoProvider INSTANCE = new DellaCargoProvider();

    private DellaCargoProvider() {

    }

    @Override
    public Response<List<CarLoad>> search(SearchData searchData) {
        SearchRequestData requestData = converter.requestData(searchData);
        int pageCount = requestData.getLimit() / RECORDS_PER_PAGE;

        List<CarLoad> totalData = new ArrayList<>(searchData.getLimit());
        for (int page = 0; page < pageCount; page++) {
            requestData.setPage(page);

            Connection con = builder.getSearchMethod(newConnection(), requestData);
            Response<Connection.Response> r = execute(con);
            if (r.getStatus() != Response.Status.OK) {
                return ResponseHelper.error(r.getStatusMessage());
            }

            Connection.Response response = r.getResult();
            try {
                List<CarLoad> data = carLoadParser.parse(response.parse());
                totalData.addAll(data);
            } catch (Throwable e) {
                return ResponseHelper.error("Error occured during searching cargo!\n" + e.getClass().getName() + ": " + e.getMessage());
            }
        }
        return ResponseHelper.ok(totalData);
    }

    @Override
    public Response<?> logout(LogoutData logoutData) {
        if (cookieId == null) {
            return ResponseHelper.error("Error occured during logout!\nCookie id can not be null!");
        }

        DellaLogoutRequestData data = (DellaLogoutRequestData) converter.requestData(logoutData);
        data.setCookieId(cookieId);

        Connection con = builder.getLogoutMethod(newConnection(), data);
        Response<Connection.Response> r = execute(con);
        if (r.getStatus() == Response.Status.OK) {
            cookies = new HashMap<>();
            cookieId = null;
            return ResponseHelper.ok();
        }
        return ResponseHelper.error(r.getStatusMessage());
    }

    @Override
    public Response<?> login(LoginData loginData) {
        Response<Boolean> loggedInR = checkLoggedIn();
        if (loggedInR.getStatus() == Response.Status.OK && loggedInR.getResult()) {
            return ResponseHelper.ok();
        }

        Connection con = builder.getLoginMethod(newConnection(), converter.requestData(loginData));
        Response<Connection.Response> r = execute(con);
        if (r.getStatus() != Response.Status.OK) {
            return ResponseHelper.error(r.getStatusMessage());
        }

        Response<String> cookieR = loginParser.parse(r.getResult());
        if (cookieR.getStatus() == Response.Status.OK) {
            cookieId = cookieR.getResult();
            return ResponseHelper.ok();
        }

        return ResponseHelper.error(cookieR.getStatusMessage());
    }

    private Response<Boolean> checkLoggedIn() {
        Connection con = builder.getCheckLoggedMethod(newConnection());
        Response<Connection.Response> checkR = execute(con);

        try {
            Document document = checkR.getResult().parse();
            String cookieId = getCookieId(document);
            if (!StringUtils.isEmpty(cookieId)) {
                this.cookieId = cookieId;
                return ResponseHelper.ok(true);
            }
        } catch (Throwable e) { }

        return ResponseHelper.ok(false);
    }

    private String getCookieId(Document document) {
        return document.select("a#exit_link").attr("onclick").split("\'")[3];
    }
}
