package burchik.bsuir.carload.bean;

import burchik.bsuir.carload.engine.provider.CargoProviderType;

public enum TransportTypeEnum {
    LYUBOJ("любой", "0"),
    TENT("тент", "11"),
    KRYTAJA("крытая", "21"),
    IZOTERM("изотерм", "4"),
    CELNOMET("цельномет.", "19"),
    REFRIZHERATOR("рефрижератор", "10"),
    AVTOBUS_GRUZOPASSAZHIR("автобус грузопас.", "29"),
    AVTOBUS_LUKS("автобус люкс", "30"),
    AVTOVOZ("автовоз", "17"),
    AVTOKRAN("автокран", "23"),
    BENZOVOZ("бензовоз", "39"),
    BETONOSMESITEL("бетоносмеситель", "50"),
    BITUMOVOZ("битумовоз", "42"),
    BORTOVAJA("бортовая", "7"),
    ZERNOVOZ("зерновоз", "3"),
    KONTEJNER_PUSTOJ("контейнер пустой", "54"),
    KONTEJNEROVOZ("контейнеровоз", "24"),
    KORMOVOZ("кормовоз", "53"),
    LESOVOZ("лесовоз", "5"),
    MASLOVOZ("масловоз", "40"),
    MEBLEVOZ("меблевоз", "36"),
    MIKROAVTOBUS("микроавтобус", "34"),
    MUKOVOZ("муковоз", "44"),
    NEGABARIT("негабарит", "33"),
    OTKRYTAJA("открытая", "8"),
    PANELEVOZ("панелевоз", "47"),
    PLATFORMA("платформа", "9"),
    PTICEVOZ("птицевоз", "52"),
    SAMOSVAL("самосвал", "22"),
    SKOTOVOZ("скотовоз", "38"),
    SPECMASHINA("спецмашина", "37"),
    STEKLOVOZ("стекловоз", "48"),
    TRAL("трал", "31"),
    TRUBOVOZ("трубовоз", "35"),
    TYAGACH("тягач", "28"),
    CELNOPLASTIC("цельнопластик", "20"),
    CEMENTOVOZ("цементовоз", "32"),
    CISTERNA_GAZOVAJA("цистерна газовая", "49"),
    CISTERN_IZOTERM("цистерна изотерм.", "51"),
    CISTERNA_PISCHEVAJA("цистерна пищ.", "2"),
    CISTERNA_HIMICHESKAJA("цистерна хим.", "14"),
    EVAKUATOR("эвакуатор", "41"),
    EKSKAVATOR("экскаватор", "43");

    private final String name;
    private final String dellaCode;

    TransportTypeEnum(String name, String dellaCode) {
        this.name = name;
        this.dellaCode = dellaCode;
    }

    public String getName() {
        return name;
    }

    public String getCode(CargoProviderType type) {
        switch (type) {
            case DELLA:
                return dellaCode;
            default:
                throw new IllegalArgumentException("Unsupported provider type " + type);
        }
    }

    public static TransportTypeEnum fromCode(String dellaCode) {
        for (TransportTypeEnum te: TransportTypeEnum.values()) {
            if (te.dellaCode.equals(dellaCode)) {
                return te;
            }
        }
        throw new IllegalArgumentException("Unsupported transport type code " + dellaCode);
    }

    public static TransportTypeEnum fromOrdinal(int transportType) {
        return TransportTypeEnum.values()[transportType];
    }
}
