package burchik.bsuir.carload.data.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import burchik.bsuir.carload.R;
import burchik.bsuir.carload.bean.CarLoad;

/**
 * Created by Candentira on 24.05.2015.
 */
public class CarLoadListAdapter extends ArrayAdapter<CarLoad> {
    private final List<CarLoad> data;

    public CarLoadListAdapter(Context context, List<CarLoad> objects) {
        super(context, R.layout.load_details, objects);
        this.data = objects;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.load_details, null);
        }
        int backgroundColor = convertView.getDrawingCacheBackgroundColor();
        if (data.get(position) != null) {
            TextView tvDate = (TextView) convertView.findViewById(R.id.date);
            TextView tvFrom = (TextView) convertView.findViewById(R.id.from);
            TextView tvTo = (TextView) convertView.findViewById(R.id.to);
            TextView tvType = (TextView) convertView.findViewById(R.id.type);
            TextView tvWeight = (TextView) convertView.findViewById(R.id.weight);
            TextView tvSize = (TextView) convertView.findViewById(R.id.size);

            tvDate.setText(data.get(position).getDate());
            tvFrom.setText(data.get(position).getFrom().toString().replace("[", "").replace("]", ""));
            tvTo.setText(data.get(position).getTo().toString().replace("[", "").replace("]", ""));
            tvType.setText(data.get(position).getType());
//            String weight = "";
//            if (data.get(position).getWeight() != null) {
//                weight = Double.toString(data.get(position).getWeight());
//                weight += " т";
//            }
            tvWeight.setText(data.get(position).getWeight());
            tvSize.setText(data.get(position).getSize() != null ? data.get(position).getSize().toString() : "");
            if(!data.get(position).getWasShown()){
                convertView.setBackgroundColor(Color.parseColor("#B6CBD5"));
            }else{
                convertView.setBackgroundColor(backgroundColor);
            }

        }
        return convertView;
    }

    /*public void swapItems(List<CarLoad> data) {
        this.data = data;
        notifyDataSetChanged();
    }*/
}
