package burchik.bsuir.carload.engine.provider.della.parser;

import org.jsoup.Connection;

import burchik.bsuir.carload.engine.data.response.Response;
import burchik.bsuir.carload.engine.data.response.ResponseHelper;
import burchik.bsuir.carload.engine.provider.HttpOutputParser;

public class DellaLoginParser implements HttpOutputParser<Connection.Response, Response<String>> {

    @Override
    public Response<String> parse(Connection.Response r) {
        String cookieId = null;
        try {
            cookieId = r.parse().select("a#exit_link").attr("onclick").split("\'")[3];
        } catch (Throwable e) {
            return ResponseHelper.error("Введен неверный логин или пароль!");
        }

        return ResponseHelper.ok(cookieId);
    }
}
