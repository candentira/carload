package burchik.bsuir.carload.activity.fragment;

import android.os.Bundle;
import android.preference.PreferenceFragment;

import burchik.bsuir.carload.R;

/**
 * Created by Candentira on 01.06.2015.
 */
public class CarLoadNotificationPreferenceFragment extends PreferenceFragment {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.pref_notif);
    }
}
