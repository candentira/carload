package burchik.bsuir.carload.engine.data.request;

public class LoginRequestData implements RequestData {
    private String login;
    private String password;

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "LoginRequestData{" +
                "login='" + login + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
