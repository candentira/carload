package burchik.bsuir.carload.engine.data.request;

import java.util.List;

public class SearchRequestData implements RequestData {
    private List<Address> addressFromList;
    private List<Address> addressToList;
    private String dateFrom;
    private String dateTo;
    private String weightFrom;
    private String weightTo;
    private String volumeFrom;
    private String volumeTo;
    private List<String> transportTypes;
    private int limit;
    private int page;

    public static class Address {
        private String country;
        private String area;
        private String city;

        public String getCountry() {
            return country;
        }

        public void setCountry(String country) {
            this.country = country;
        }

        public String getArea() {
            return area;
        }

        public void setArea(String area) {
            this.area = area;
        }

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }
    }

    public List<Address> getAddressFromList() {
        return addressFromList;
    }

    public void setAddressFromList(List<Address> addressFromList) {
        this.addressFromList = addressFromList;
    }

    public List<Address> getAddressToList() {
        return addressToList;
    }

    public void setAddressToList(List<Address> addressToList) {
        this.addressToList = addressToList;
    }

    public String getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(String dateFrom) {
        this.dateFrom = dateFrom;
    }

    public String getDateTo() {
        return dateTo;
    }

    public void setDateTo(String dateTo) {
        this.dateTo = dateTo;
    }

    public String getWeightFrom() {
        return weightFrom;
    }

    public void setWeightFrom(String weightFrom) {
        this.weightFrom = weightFrom;
    }

    public String getWeightTo() {
        return weightTo;
    }

    public void setWeightTo(String weightTo) {
        this.weightTo = weightTo;
    }

    public String getVolumeFrom() {
        return volumeFrom;
    }

    public void setVolumeFrom(String volumeFrom) {
        this.volumeFrom = volumeFrom;
    }

    public String getVolumeTo() {
        return volumeTo;
    }

    public void setVolumeTo(String volumeTo) {
        this.volumeTo = volumeTo;
    }

    public List<String> getTransportTypes() {
        return transportTypes;
    }

    public void setTransportTypes(List<String> transportTypes) {
        this.transportTypes = transportTypes;
    }

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    @Override
    public String toString() {
        return "SearchRequestData{" +
                "addressFromList=" + addressFromList +
                ", addressToList=" + addressToList +
                ", dateFrom='" + dateFrom + '\'' +
                ", dateTo='" + dateTo + '\'' +
                ", weightFrom='" + weightFrom + '\'' +
                ", weightTo='" + weightTo + '\'' +
                ", volumeFrom='" + volumeFrom + '\'' +
                ", volumeTo='" + volumeTo + '\'' +
                ", transportTypes=" + transportTypes +
                ", limit=" + limit +
                ", page=" + page +
                '}';
    }
}
