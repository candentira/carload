package burchik.bsuir.carload.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.telephony.SmsManager;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.apache.commons.lang3.StringUtils;
import org.jsoup.helper.StringUtil;

import java.util.concurrent.ExecutionException;

import burchik.bsuir.carload.R;
import burchik.bsuir.carload.bean.CarLoad;
import burchik.bsuir.carload.engine.data.response.Response;

/**
 * Created by Candentira on 21.06.2015.
 */
public class DetailedCarLoadActivity extends AppCompatActivity {

    SharedPreferences sharedPreferences;
    boolean hasPhoneNumber = false;
    String phoneNumber = "+375296212562";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detailed_car_load);

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);

        Bundle bundle = getIntent().getExtras();
        CarLoad carLoad = bundle.getParcelable(LoadsListActivity.CAR_LOAD);
        setCarLoadValues(carLoad);
    }

    private void setCarLoadValues(CarLoad carLoad) {
        TextView tvDate = (TextView) findViewById(R.id.date);
        TextView tvFrom = (TextView) findViewById(R.id.from);
        TextView tvTo = (TextView) findViewById(R.id.to);
        TextView tvDistance = (TextView) findViewById(R.id.distance);
        TextView tvType = (TextView) findViewById(R.id.type);
        TextView tvWeight = (TextView) findViewById(R.id.weight);
        TextView tvSize = (TextView) findViewById(R.id.size);
        TextView tvDescription = (TextView) findViewById(R.id.description);
        TextView tvCreated = (TextView) findViewById(R.id.created);
        TextView tvChanged = (TextView) findViewById(R.id.changed);
        TextView tvPrice = (TextView) findViewById(R.id.price);
        TextView tvPhoneNumber = (TextView) findViewById(R.id.phoneNumber);
        TextView tvContactName = (TextView) findViewById(R.id.contactName);
        TextView tvContactEmail = (TextView) findViewById(R.id.contactEmail);
        TextView tvContactSkype = (TextView) findViewById(R.id.contactSkype);

        LinearLayout llPhone = (LinearLayout) findViewById(R.id.layout_phone);
        if(!StringUtils.isEmpty(carLoad.getContactPhoneString())){
            tvPhoneNumber.setText(carLoad.getContactPhoneString());
            hasPhoneNumber = true;
            phoneNumber = carLoad.getContactPhoneString();
        }else{
            llPhone.setVisibility(LinearLayout.GONE);
        }
        LinearLayout llContactName = (LinearLayout) findViewById(R.id.layout_contact_name);
        if(!StringUtils.isEmpty(carLoad.getContactName())){
            tvContactName.setText(carLoad.getContactName());
        }else{
            llContactName.setVisibility(LinearLayout.GONE);
        }
        LinearLayout llContactEmail = (LinearLayout) findViewById(R.id.layout_contact_email);
        if(!StringUtils.isEmpty(carLoad.getContactEmail())){
            tvContactEmail.setText(carLoad.getContactEmail());
        }else{
            llContactEmail.setVisibility(LinearLayout.GONE);
        }
        LinearLayout llContactSkype = (LinearLayout) findViewById(R.id.layout_contact_skype);
        if(!StringUtils.isEmpty(carLoad.getContactSkype())){
            tvContactSkype.setText(carLoad.getContactSkype());
        }else{
            llContactSkype.setVisibility(LinearLayout.GONE);
        }
        byte[] catloadContactPhoneStr = carLoad.getContactPhonePicture();
        if(catloadContactPhoneStr != null && catloadContactPhoneStr.length > 0){
            ImageView ivPhoneNumberPicture = (ImageView) findViewById(R.id.phoneNumberPicture);
            Bitmap bmp;
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inMutable = true;
            bmp = BitmapFactory.decodeByteArray(catloadContactPhoneStr, 0, catloadContactPhoneStr.length, options);
            //Canvas canvas = new Canvas(bmp); // now it should work ok
            ivPhoneNumberPicture.setImageBitmap(bmp);

        }

        tvDate.setText(carLoad.getDate());
        tvFrom.setText(carLoad.getFrom().toString().replace("[", "").replace("]", ""));
        tvTo.setText(carLoad.getTo().toString().replace("[", "").replace("]", ""));
        tvDistance.setText(carLoad.getDistance());
        tvType.setText(carLoad.getType());
        tvWeight.setText(carLoad.getWeight());
        tvSize.setText(carLoad.getSize());
        tvDescription.setText(carLoad.getDescription());
        tvCreated.setText(carLoad.getCreated());
        tvChanged.setText(carLoad.getChanged());
        LinearLayout llPrice = (LinearLayout) findViewById(R.id.layout_price);
        if(!StringUtils.isEmpty(carLoad.getPrice())){
            tvPrice.setText(carLoad.getPrice());
        }else{
            llPrice.setVisibility(LinearLayout.GONE);
        }

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        if(hasPhoneNumber){
            getMenuInflater().inflate(R.menu.menu_detailed, menu);
        }
        return super.onCreateOptionsMenu(menu);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        if(hasPhoneNumber) {
            int id = item.getItemId();
            switch (id) {
                case R.id.call:
                    Intent callIntent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:"
                            + phoneNumber));
                    startActivity(callIntent);
                    break;
                case R.id.send_sms:
                    Intent smsIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("sms:"
                            + phoneNumber));
                    String smsTemplate = sharedPreferences.getString(getString(R.string.sms_template_key), "");
                    if (smsTemplate != null && smsTemplate != "") {
                        smsIntent.putExtra("sms_body", smsTemplate);
                    }
                    startActivity(smsIntent);
                /*SmsManager smsManager = SmsManager.getDefault();
                smsManager.sendTextMessage("+375296210380", null, "sms message", null, null);*/
                    break;
                default:
                    //Do nothing
            }
        }
            return super.onOptionsItemSelected(item);
    }

}
