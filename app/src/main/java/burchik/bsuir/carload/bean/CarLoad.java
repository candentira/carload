package burchik.bsuir.carload.bean;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Candentira on 24.05.2015.
 */
public class CarLoad implements Parcelable{
    private String id;
    private String date;
    private List<CargoAddress> from;
    private List<CargoAddress> to;
    private String type;
    private String size;
    private String price;
    private String weight;
    private String description;
    private String distance;
    private String changed;
    private String created;
    private String link;
    private String contactPhoneString;
    private byte[] contactPhonePicture = new byte[0];
    private String contactName;
    private String contactEmail;
    private String contactSkype;
    private boolean wasShown = false;

    public CarLoad() {
        from = new ArrayList<CargoAddress>();
        to = new ArrayList<CargoAddress>();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public List<CargoAddress> getFrom() {
        return from;
    }

    public void setFrom(List<CargoAddress> from) {
        this.from = from;
    }

    public List<CargoAddress> getTo() {
        return to;
    }

    public void setTo(List<CargoAddress> to) {
        this.to = to;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }


    public boolean getWasShown() {
        return wasShown;
    }

    public void setWasShown(boolean wasShown) {
        this.wasShown = wasShown;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getChanged() {
        return changed;
    }

    public void setChanged(String changed) {
        this.changed = changed;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getContactPhoneString() {
        return contactPhoneString;
    }

    public void setContactPhoneString(String contactPhoneString) {
        this.contactPhoneString = contactPhoneString;
    }

    public String getContactName() {
        return contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    public String getContactEmail() {
        return contactEmail;
    }

    public void setContactEmail(String contactEmail) {
        this.contactEmail = contactEmail;
    }

    public String getContactSkype() {
        return contactSkype;
    }

    public void setContactSkype(String contactSkype) {
        this.contactSkype = contactSkype;
    }

    public byte[] getContactPhonePicture() {
        return contactPhonePicture;
    }

    public void setContactPhonePicture(byte[] contactPhonePicture) {
        this.contactPhonePicture = contactPhonePicture;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(date);
        dest.writeTypedList(from);
        dest.writeTypedList(to);
        dest.writeString(type);
        dest.writeString(size);
        dest.writeString(price);
        dest.writeString(weight);
        dest.writeString(description);
        dest.writeString(distance);
        dest.writeString(changed);
        dest.writeString(created);
        dest.writeString(link);
        dest.writeString(contactPhoneString);
        dest.writeInt(contactPhonePicture.length);
        dest.writeByteArray(contactPhonePicture);
        dest.writeString(contactEmail);
        dest.writeString(contactName);
        dest.writeString(contactSkype);
        dest.writeByte((byte) (wasShown ? 1 : 0));
    }

    public static final Parcelable.Creator<CarLoad> CREATOR
            = new Parcelable.Creator<CarLoad>() {
        public CarLoad createFromParcel(Parcel in) {
            return new CarLoad(in);
        }

        public CarLoad[] newArray(int size) {
            return new CarLoad[size];
        }
    };

    private CarLoad(Parcel in) {
        this();
        id = in.readString();
        date = in.readString();
        in.readTypedList(from, CargoAddress.CREATOR);
        in.readTypedList(to, CargoAddress.CREATOR);
        type = in.readString();
        size = in.readString();
        price = in.readString();
        weight = in.readString();
        description = in.readString();
        distance = in.readString();
        changed = in.readString();
        created = in.readString();
        link = in.readString();
        contactPhoneString = in.readString();
        contactPhonePicture = new byte[in.readInt()];
        in.readByteArray(contactPhonePicture);
        contactEmail = in.readString();
        contactName = in.readString();
        contactSkype = in.readString();
        wasShown = in.readByte() != 0;
    }

    @Override
    public String toString() {
        return "CarLoad{" +
                "id='" + id + '\'' +
                ", date='" + date + '\'' +
                ", from=" + from +
                ", to=" + to +
                ", type='" + type + '\'' +
                ", size='" + size + '\'' +
                ", price='" + price + '\'' +
                ", weight='" + weight + '\'' +
                ", description='" + description + '\'' +
                ", distance='" + distance + '\'' +
                ", changed='" + changed + '\'' +
                ", created='" + created + '\'' +
                ", link='" + link + '\'' +
                ", contactPhoneString='" + contactPhoneString + '\'' +
                ", contactPhonePicture=" + Arrays.toString(contactPhonePicture) +
                ", contactName='" + contactName + '\'' +
                ", contactEmail='" + contactEmail + '\'' +
                ", contactSkype='" + contactSkype + '\'' +
                '}';
    }
}