package burchik.bsuir.carload.engine.data.response;

public final class ResponseHelper {

    private ResponseHelper() {
        throw new AssertionError();
    }

    public static <T> Response<T> ok() {
        return new Response<>(null, Response.Status.OK, null);
    }

    public static <T> Response<T> ok(T result) {
        return new Response<>(result, Response.Status.OK, null);
    }

    public static <T> Response<T> error(String errorMsg) {
        return new Response<>(null, Response.Status.ERROR, errorMsg);
    }
}
