package burchik.bsuir.carload.bean;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Date;
import java.util.List;

/**
 * Created by Candentira on 03.06.2015.
 */
public class SearchData implements Parcelable {
    private List<PreferenceAddress> addressesFrom;
    private List<PreferenceAddress> addressesTo;
    private Date dateFrom;
    private Date dateTo;
    private int weightFrom;
    private int weightTo;
    private int volumeFrom;
    private int volumeTo;
    private TransportTypeEnum transportType;
    private int limit = 50;

    public SearchData() {
    }

    public List<PreferenceAddress> getAddressesFrom() {
        return addressesFrom;
    }

    public void setAddressesFrom(List<PreferenceAddress> addressesFrom) {
        this.addressesFrom = addressesFrom;
    }

    public List<PreferenceAddress> getAddressesTo() {
        return addressesTo;
    }

    public void setAddressesTo(List<PreferenceAddress> addressesTo) {
        this.addressesTo = addressesTo;
    }

    public Date getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(Date dateFrom) {
        this.dateFrom = dateFrom;
    }

    public Date getDateTo() {
        return dateTo;
    }

    public void setDateTo(Date dateTo) {
        this.dateTo = dateTo;
    }

    public int getWeightFrom() {
        return weightFrom;
    }

    public void setWeightFrom(int weightFrom) {
        this.weightFrom = weightFrom;
    }

    public int getWeightTo() {
        return weightTo;
    }

    public void setWeightTo(int weightTo) {
        this.weightTo = weightTo;
    }

    public int getVolumeFrom() {
        return volumeFrom;
    }

    public void setVolumeFrom(int volumeFrom) {
        this.volumeFrom = volumeFrom;
    }

    public int getVolumeTo() {
        return volumeTo;
    }

    public void setVolumeTo(int volumeTo) {
        this.volumeTo = volumeTo;
    }

    public TransportTypeEnum getTransportType() {
        return transportType;
    }

    public void setTransportType(TransportTypeEnum transportType) {
        this.transportType = transportType;
    }

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelableArray(addressesFrom.toArray(new PreferenceAddress[0]), flags);
        dest.writeParcelableArray(addressesTo.toArray(new PreferenceAddress[0]), flags);
        dest.writeLong(dateFrom.getTime());
        dest.writeLong(dateTo.getTime());
        dest.writeInt(weightFrom);
        dest.writeInt(weightTo);
        dest.writeInt(volumeFrom);
        dest.writeInt(volumeTo);
        dest.writeString(transportType.name());
        dest.writeInt(limit);
    }

    public static final Parcelable.Creator<SearchData> CREATOR
            = new Parcelable.Creator<SearchData>() {
        public SearchData createFromParcel(Parcel in) {
            return new SearchData(in);
        }

        public SearchData[] newArray(int size) {
            return new SearchData[size];
        }
    };

    private SearchData(Parcel in) {
        this();
        in.readParcelableArray(PreferenceAddress.class.getClassLoader());
        in.readParcelableArray(PreferenceAddress.class.getClassLoader());
        dateFrom = new Date(in.readLong());
        dateTo = new Date(in.readLong());
        weightFrom = in.readInt();
        weightTo = in.readInt();
        volumeFrom = in.readInt();
        volumeTo = in.readInt();
        transportType = TransportTypeEnum.valueOf(in.readString());
        limit = in.readInt();
    }

    @Override
    public int describeContents() {
        return 0;
    }
}
