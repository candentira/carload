package burchik.bsuir.carload.exception;

/**
 * Created by aklotos on 21.06.2015.
 */
public class CargoTransportServerRequestException extends RuntimeException {
    public CargoTransportServerRequestException(String detailMessage) {
        super(detailMessage);
    }

    public CargoTransportServerRequestException(String detailMessage, Throwable throwable) {
        super(detailMessage, throwable);
    }
}
