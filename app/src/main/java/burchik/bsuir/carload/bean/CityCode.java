package burchik.bsuir.carload.bean;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

public enum CityCode implements Parcelable {
    ALL(AreaCode.ALL, "Все", ""),

    // BY
    BREST(AreaCode.BRESTSKAJA, "Брест", "12037"),
    VITEBSK(AreaCode.VITEBSKAJA, "Витебск", "12039"),
    GOMEL(AreaCode.GOMELSKAJA, "Гомель", "12040"),
    GRODNO(AreaCode.GRODNENSKAJA, "Гродно", "12105"),
    MINSK(AreaCode.MINSKAJA, "Минск", "21779"),
    MOGILEV(AreaCode.MOGILEVSKAJA, "Могилев", "21788"),
    SVETLOGORSK(AreaCode.GOMELSKAJA, "Светлогорск", "125662"),

    // RU
    MOSKVA(AreaCode.MOSKOVSKAJA, "Москва", "167"),
    TULA(AreaCode.TULSKAJA, "Тула", "373"),
    CHEHOV(AreaCode.MOSKOVSKAJA, "Чехов", "390");

    private AreaCode area;
    private String name;
    private String dellaCode;

    CityCode(AreaCode area, String name, String dellaCode) {
        this.area = area;
        this.name = name;
        this.dellaCode = dellaCode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public AreaCode getArea() {
        return area;
    }

    private void setArea(AreaCode area) {
        this.area = area;
    }

    public String getDellaCode() {
        return dellaCode;
    }

    private void setDellaCode(String dellaCode) {
        this.dellaCode = dellaCode;
    }

    public static List<CityCode> getByArea(AreaCode area) {
        List<CityCode> cities = new ArrayList<>();
        cities.add(ALL);
        for (CityCode city: CityCode.values()) {
            if (city.getArea() == area) {
                cities.add(city);
            }
        }
        return cities;
    }

    public static CityCode fromOrdinal(int ordinal) {
        return CityCode.values()[ordinal];
    }

    public static final Parcelable.Creator<CityCode> CREATOR = new Parcelable.Creator<CityCode>() {
        public CityCode createFromParcel(Parcel in) {
            CityCode area = CityCode.values()[in.readInt()];
            area.setArea(AreaCode.values()[in.readInt()]);
            area.setName(in.readString());
            area.setDellaCode(in.readString());
            return area;
        }

        public CityCode[] newArray(int size) {
            return new CityCode[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel out, int flags) {
        out.writeInt(ordinal());
        out.writeInt(area.ordinal());
        out.writeString(name);
        out.writeString(dellaCode);
    }
}
