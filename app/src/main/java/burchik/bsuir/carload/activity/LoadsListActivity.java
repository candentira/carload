package burchik.bsuir.carload.activity;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.ExecutionException;

import burchik.bsuir.carload.R;
import burchik.bsuir.carload.activity.preference.SeekBarPreference;
import burchik.bsuir.carload.bean.AreaCode;
import burchik.bsuir.carload.bean.CarLoad;
import burchik.bsuir.carload.bean.CityCode;
import burchik.bsuir.carload.bean.CountryCode;
import burchik.bsuir.carload.bean.LoginData;
import burchik.bsuir.carload.bean.LogoutData;
import burchik.bsuir.carload.bean.PreferenceAddress;
import burchik.bsuir.carload.bean.SearchData;
import burchik.bsuir.carload.bean.TransportTypeEnum;
import burchik.bsuir.carload.data.DatabaseHelper;
import burchik.bsuir.carload.data.adapter.CarLoadListAdapter;
import burchik.bsuir.carload.engine.data.response.Response;
import burchik.bsuir.carload.engine.provider.della.DellaCargoProvider;


public class LoadsListActivity extends AppCompatActivity {
    private static final String TAG = "carLoad.ListActivity";

    private static final SimpleDateFormat SDF = new SimpleDateFormat("yyyy.MM.dd");

    public static final String CAR_LOAD_LIST = "carLoadList";
    public static final String CAR_LOAD = "carLoad";
    public static final String RELOAD_DELAY = "reloadDelay";
    public static final String IS_TIMER = "isTimer";
    public static final String HIDE_WAS_SHOWN = "hideWasShown";
    public static final String SEARCH_DATA = "searchData";
    public final static String BROADCAST_ACTION = "ru.startandroid.develop.p0961servicebackbroadcast";
    public static final String NEW_CARLOAD_AMOUNT = "newCarloadAmount";
    public final int DEFAULT_RELOAD_DELAY = 3;

    public ArrayList<CarLoad> carLoadList = new ArrayList<CarLoad>();
    private CarLoadListAdapter adapter;
    private ListView lv;
    SwipeRefreshLayout mSwipeRefreshLayout;
    boolean isLogged = false;
    boolean hideWasShown = false;

    SharedPreferences sharedPreferences;
    BroadcastReceiver br;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.v(TAG, "in onCreate");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loads_list);

        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.activity_main_swipe_refresh_layout);
        lv = (ListView) findViewById(R.id.listView);
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        adapter = new CarLoadListAdapter(this, carLoadList);
        lv.setAdapter(adapter);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                CarLoad listItem = (CarLoad) lv.getItemAtPosition(position);
                listItem.setWasShown(true);
                DatabaseHelper.getInstance(LoadsListActivity.this).updateCarLoadShown(listItem);
                Intent intent = new Intent(LoadsListActivity.this, DetailedCarLoadActivity.class);
                intent.putExtra(CAR_LOAD, listItem);
                startActivity(intent);
            }
        });
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                startUpdateNotificationService(getSearchData(), false);
                mSwipeRefreshLayout.setRefreshing(false);
            }
        });

        startUpdateNotificationService(getSearchData(), true);

        br = new BroadcastReceiver() {
            public void onReceive(Context context, Intent intent) {
                Log.d(TAG, "BroadcastReceiver:onReceive");
                carLoadList.clear();
                carLoadList.addAll(intent.<CarLoad>getParcelableArrayListExtra(LoadsListActivity.CAR_LOAD_LIST));
                adapter.notifyDataSetChanged();
            }
        };
        IntentFilter intFilt = new IntentFilter(BROADCAST_ACTION);
        registerReceiver(br, intFilt);
        isLogged = false;

        //DataLoader.setSearchUrl(getSearchData());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_list, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    protected void onResume() {
        SearchData searchData = getSearchData();
        //DataLoader.setSearchUrl(searchUrl);
        startUpdateNotificationService(searchData, false);
        super.onResume();
    }

    private SearchData getSearchData() {
        SearchData searchData = null;
        if (sharedPreferences != null) {
            int fromCountry = sharedPreferences.getInt(getString(R.string.from_country_key), CountryCode.ALL.ordinal());
            int toCountry = sharedPreferences.getInt(getString(R.string.to_country_key), CountryCode.ALL.ordinal());
            int fromArea = sharedPreferences.getInt(getString(R.string.from_region_key), AreaCode.ALL.ordinal());
            int toArea = sharedPreferences.getInt(getString(0/*to_region_key*/), AreaCode.ALL.ordinal());
            int fromCity = sharedPreferences.getInt(getString(0/*from_city_key*/), CityCode.ALL.ordinal());
            int toCity = sharedPreferences.getInt(getString(0/*to_city_key*/), CityCode.ALL.ordinal());

            String fromDate = sharedPreferences.getString(getString(R.string.from_time_key), "");
            String toDate = sharedPreferences.getString(getString(R.string.till_time_key), "");
            int fromWeight = sharedPreferences.getInt(getString(R.string.from_weight_key), SeekBarPreference.DEFAULT_MIN_VALUE);
            int toWeight = sharedPreferences.getInt(getString(R.string.to_weight_key), SeekBarPreference.DEFAULT_MIN_VALUE);
            int fromVolume = sharedPreferences.getInt(getString(R.string.from_volume_key), SeekBarPreference.DEFAULT_MIN_VALUE);
            int toVolume = sharedPreferences.getInt(getString(R.string.to_volume_key), SeekBarPreference.DEFAULT_MIN_VALUE);
            int transportType = sharedPreferences.getInt(getString(R.string.transport_type_key), TransportTypeEnum.LYUBOJ.ordinal());
            try {
                PreferenceAddress from = new PreferenceAddress(
                        CountryCode.fromOrdinal(fromCountry),
                        AreaCode.fromOrdinal(fromArea),
                        CityCode.fromOrdinal(fromCity));
                PreferenceAddress to = new PreferenceAddress(
                        CountryCode.fromOrdinal(toCountry),
                        AreaCode.fromOrdinal(toArea),
                        CityCode.fromOrdinal(toCity));

                searchData = new SearchData();
                searchData.setAddressesFrom(Arrays.asList(from));
                searchData.setAddressesTo(Arrays.asList(to));
                searchData.setDateFrom(SDF.parse(fromDate));
                searchData.setDateTo(SDF.parse(toDate));
                searchData.setWeightFrom(fromWeight);
                searchData.setWeightTo(toWeight);
                searchData.setVolumeFrom(fromVolume);
                searchData.setVolumeTo(toVolume);
                searchData.setTransportType(TransportTypeEnum.fromOrdinal(transportType));
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        return searchData;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        switch (id) {
            case R.id.settings:
                startActivity(new Intent(this, PrefActivity.class));
                break;
            case R.id.update:
                startUpdateNotificationService(getSearchData(), false);
                break;
            case R.id.do_login:
                try {
                    if(!isLogged) {
                        Response<?> loginResponse = new DoAsynkLogin().execute(getLoginData()).get();
                        sendLoginMessage(loginResponse.getStatus());
                    }else{
                        Response<?> logoutResponse = new DoAsynkLogout().execute(getLogoutData()).get();
                        sendLogoutMessage(logoutResponse.getStatus());
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                }
                break;
            default:
                //Do nothing
        }
        return super.onOptionsItemSelected(item);
    }

    private LoginData getLoginData() {
        String dellaLogin = sharedPreferences.getString(getString(R.string.della_login_key), "");
        String dellaPassword = sharedPreferences.getString(getString(R.string.della_password_key), "");
        return new LoginData(dellaLogin, dellaPassword);
    }

    private LogoutData getLogoutData() {
        String dellaLogin = sharedPreferences.getString(getString(R.string.della_login_key), "");
        return new LogoutData(dellaLogin);
    }

    private void sendLoginMessage(Response.Status status) {
        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(this).setTitle(getText(R.string.login_title));
        if (Response.Status.OK.equals(status)) {
            alertBuilder.setMessage(R.string.successful_login);
            isLogged = true;
            startUpdateNotificationService(getSearchData(), false);
        } else {
            alertBuilder.setMessage(R.string.unsuccessful_login);
        }
        alertBuilder.show();
    }

    private void sendLogoutMessage(Response.Status status) {
        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(this).setTitle(getText(R.string.logout_title));
        if (Response.Status.OK.equals(status)) {
            alertBuilder.setMessage(R.string.successful_logout);
            isLogged = false;
            startUpdateNotificationService(getSearchData(), false);
        } else {
            alertBuilder.setMessage(R.string.unsuccessful_logout);
        }
        alertBuilder.show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(br);
        stopService(new Intent(this, UpdateNotificationService.class));
        try {
            Response<?> logoutResponse = new DoAsynkLogout().execute(getLogoutData()).get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
    }

    public void startUpdateNotificationService(SearchData searchData, boolean isTimer) {
        int reloadDelay = sharedPreferences.getInt(getString(R.string.update_delay), DEFAULT_RELOAD_DELAY);
        Intent serviceIntent = new Intent(this, UpdateNotificationService.class);
        serviceIntent.putExtra(RELOAD_DELAY, reloadDelay);
        serviceIntent.putExtra(SEARCH_DATA, searchData);
        serviceIntent.putExtra(IS_TIMER, isTimer);
        serviceIntent.putExtra(HIDE_WAS_SHOWN, hideWasShown);
        //serviceIntent.putParcelableArrayListExtra(CAR_LOAD_LIST, carLoadList);
        startService(serviceIntent);
    }

    private class DoAsynkLogin extends AsyncTask<LoginData, Void, Response<?>> {
        @Override
        protected Response<?> doInBackground(LoginData... params) {
            return DellaCargoProvider.INSTANCE.login(params[0]);
        }
    }

    private class DoAsynkLogout extends AsyncTask<LogoutData, Void, Response<?>> {
        @Override
        protected Response<?> doInBackground(LogoutData... params) {
            return DellaCargoProvider.INSTANCE.logout(params[0]);
        }
    }
}
