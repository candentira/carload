package burchik.bsuir.carload.activity.preference;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;

/**
 * Created by Candentira on 21.06.2015.
 */
public class EditTextPreference extends android.preference.EditTextPreference {
    private static final String TAG = "carLoad.ListActivity";

    private boolean isPassword;

    public EditTextPreference(Context context, AttributeSet attrs) {
        super(context, attrs);
        for (int i=0;i<attrs.getAttributeCount();i++) {
            String attr = attrs.getAttributeName(i);
            String val  = attrs.getAttributeValue(i);
            if (attr.equalsIgnoreCase("inputType")) {
                isPassword = "0x81".equals(val);
            }
        }
    }

    @Override
    protected void onDialogClosed(boolean positiveResult) {
        super.onDialogClosed(positiveResult);
        setSummary(getSummary());
    }

    @Override
    public CharSequence getSummary() {
        CharSequence result = this.getText();
        if(isPassword && result != null){
            result = result.toString().replaceAll(".", "*");
        }
        return result;
    }
}
