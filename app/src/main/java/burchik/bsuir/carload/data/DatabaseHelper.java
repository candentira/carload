package burchik.bsuir.carload.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

import burchik.bsuir.carload.bean.CarLoad;

/**
 * Created by Candentira on 06.06.2015.
 */
public class DatabaseHelper extends SQLiteOpenHelper {

    private static DatabaseHelper sInstance;

    private static final String DATABASE_NAME = "carload.db";
    private static final String CARLOAD_TABLE_NAME = "carload";
    private static final String SIZE_TABLE_NAME = "size";
    private static final String ADDRESS_TABLE_NAME = "address";

    private static final int DATABASE_VERSION = 1;
    static final String VALUE = "value";
    static final String TABLE = "constants";

    private static final String CARLOAD_ID = "_id";
    private static final String CARLOAD_WAS_SHOWN = "wasShown";
    private static final String CARLOAD_CREATE_TABLE = "CREATE TABLE " + CARLOAD_TABLE_NAME +
            " (" + CARLOAD_ID + " TEXT PRIMARY KEY, " +
            CARLOAD_WAS_SHOWN + " TEXT);";



    private DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public static synchronized DatabaseHelper getInstance(Context context) {

        // Use the application context, which will ensure that you
        // don't accidentally leak an Activity's context.
        // See this article for more information: http://bit.ly/6LRzfx
        if (sInstance == null) {
            sInstance = new DatabaseHelper(context.getApplicationContext());
        }
        return sInstance;
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        /*db.execSQL(SIZE_CREATE_TABLE);
        db.execSQL(ADDRESS_CREATE_TABLE);*/
        db.execSQL(CARLOAD_CREATE_TABLE);

        /*ContentValues cv = new ContentValues();
        cv.put(TITLE, "Gravity, Death Star I");
        //cv.put(VALUE, SensorManager.GRAVITY_DEATH_STAR_I);
        db.insert("constants", TITLE, cv);
        cv.put(TITLE, "Gravity, Earth");
        //cv.put(VALUE, SensorManager.GRAVITY_EARTH);
        db.insert("constants", TITLE, cv);*/
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        super.onDowngrade(db, oldVersion, newVersion);
    }

    /**
     * All CRUD(Create, Read, Update, Delete) Operations
     */

    // Adding new carLoad
    public void addCarLoad(CarLoad carLoad) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(CARLOAD_ID, carLoad.getId());
        values.put(CARLOAD_WAS_SHOWN, carLoad.getWasShown());

        // Inserting Row
        db.insert(CARLOAD_TABLE_NAME, null, values);
    }

    // Getting single carLoad
    public CarLoad getCarLoad(String id) {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(CARLOAD_TABLE_NAME, new String[]{CARLOAD_ID, CARLOAD_WAS_SHOWN},
                CARLOAD_ID + "=?",
                new String[]{id}, null, null, null);
        CarLoad carLoad = null;
        if (cursor != null && cursor.moveToNext()) {

            carLoad = new CarLoad();
            carLoad.setId(cursor.getString(0));
            carLoad.setWasShown(cursor.getInt(1) > 0);
        }
        cursor.close();
        return carLoad;
        /*SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(CARLOAD_TABLE_NAME, new String[]{CARLOAD_ID, CARLOAD_DATE,
                        CARLOAD_TYPE, CARLOAD_WEIGHT, CARLOAD_PRICE,}, CARLOAD_ID + "=?",
                new String[]{String.valueOf(id)}, null, null, null);
        CarLoad carLoad = null;
        if (cursor != null && cursor.moveToNext()) {

            carLoad = new CarLoad();
            carLoad.setId(cursor.getInt(0));
            carLoad.setDate(cursor.getString(1));
            carLoad.setType(cursor.getString(2));
            carLoad.setWeight(cursor.getDouble(3));
            carLoad.setPrice(cursor.getString(4));
        }
        cursor.close();
        db.close();
        return carLoad;*/
    }

    /*public CarLoad getCarLoad(String date, String type, double weight, String price) {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(CARLOAD_TABLE_NAME, new String[]{CARLOAD_ID, CARLOAD_DATE, CARLOAD_TYPE, CARLOAD_WEIGHT, CARLOAD_PRICE,},
                CARLOAD_DATE + "=? AND " + CARLOAD_TYPE + "=? AND " + CARLOAD_WEIGHT + "=? AND " + CARLOAD_PRICE + "=?",
                new String[]{date, type, String.valueOf(weight), price}, null, null, null);
        CarLoad carLoad = null;
        if (cursor != null && cursor.moveToNext()) {

            carLoad = new CarLoad();
            carLoad.setId(cursor.getInt(0));
            carLoad.setDate(cursor.getString(1));
            carLoad.setType(cursor.getString(2));
            carLoad.setWeight(cursor.getDouble(3));
            carLoad.setPrice(cursor.getString(4));
        }
        cursor.close();
        db.close();
        return carLoad;
    }*/

    // Getting All CarLoads
    public List<CarLoad> getAllCarLoads() {
        List<CarLoad> carLoadList = new ArrayList<CarLoad>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + CARLOAD_TABLE_NAME;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                CarLoad carLoad = new CarLoad();
                carLoad.setId(cursor.getString(0));
                carLoad.setWasShown(cursor.getInt(1) > 0);
                // Adding carLoad to list
                carLoadList.add(carLoad);
            } while (cursor.moveToNext());
        }
        cursor.close();
        return carLoadList;
        
        /*List<CarLoad> carLoadList = new ArrayList<CarLoad>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + CARLOAD_TABLE_NAME;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                CarLoad carLoad = new CarLoad();
                carLoad.setId(cursor.getInt(0));
                carLoad.setDate(cursor.getString(1));
                carLoad.setType(cursor.getString(2));
                carLoad.setWeight(cursor.getDouble(3));
                carLoad.setPrice(cursor.getString(4));
                // Adding carLoad to list
                carLoadList.add(carLoad);
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
        return carLoadList;*/
    }

    /*// Updating single carLoad
    public int updateCarLoad(CarLoad carLoad) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(CARLOAD_DATE, carLoad.getDate());
        values.put(CARLOAD_TYPE, carLoad.getType());
        values.put(CARLOAD_WEIGHT, carLoad.getWeight());
        values.put(CARLOAD_PRICE, carLoad.getPrice());

        db.close();
        return db.update(CARLOAD_TABLE_NAME, values, CARLOAD_ID + " = ?",
                new String[]{String.valueOf(carLoad.getId())});
    }*/

    // Updating single carLoad
    public int updateCarLoadShown(String id, boolean wasShown) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(CARLOAD_WAS_SHOWN, wasShown);
        int updatedAmount = db.update(CARLOAD_TABLE_NAME, values, CARLOAD_ID + " = ?",
                new String[]{id});

        return updatedAmount;
    }

    public int updateCarLoadShown(CarLoad carLoad) {
        return updateCarLoadShown(carLoad.getId(), carLoad.getWasShown());
    }
    
    // Deleting single carLoad
    public void deleteCarLoad(int id) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(CARLOAD_TABLE_NAME, CARLOAD_ID + " = ?",
                new String[]{String.valueOf(id)});
    }


    // Getting carLoads Count
    public int getCarLoadsCount() {
        String countQuery = "SELECT  * FROM " + CARLOAD_TABLE_NAME;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        int count = cursor.getCount();
        cursor.close();
        return count;
    }

    public void deleteCarLoads() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(CARLOAD_TABLE_NAME, null, null);
    }
}
