package burchik.bsuir.carload.data;

import android.os.AsyncTask;
import android.util.Log;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import burchik.bsuir.carload.bean.CarLoad;
import burchik.bsuir.carload.bean.SearchData;
import burchik.bsuir.carload.engine.data.response.Response;
import burchik.bsuir.carload.engine.provider.della.DellaCargoProvider;

/**
 * Created by Candentira on 06.06.2015.
 */
public class DataLoader extends AsyncTask<SearchData, Void, List<CarLoad>> {
    private static final String TAG = "carLoad.DataLoader";

    //public static String BASE_SEARCH_URL = "http://www.della.by/search/";
    //public static String SEARCH_URL = "http://www.della.by/search/";

    //public List<CarLoad> carLoadList = new ArrayList<CarLoad>();

    @Override
    protected List<CarLoad> doInBackground(SearchData... searchData) {
        Response<List<CarLoad>> r = DellaCargoProvider.INSTANCE.search(searchData[0]);
        if (r.getStatus() == Response.Status.ERROR) {
            // TODO: show view with error r.getStatusMessage()
            System.out.println("r.getStatusMessage() = " + r.getStatusMessage());
            return Collections.emptyList();
        } else {
            return r.getResult();
        }

        /*Log.v(TAG, "in doInBackground");

        Document doc;
        String id;
        Elements elements;
        Elements fromTo;
        Elements date;
        Elements type;
        Elements weight;
        Elements size;
        Elements price;

        try {
            doc = Jsoup.connect(SEARCH_URL).get();
            elements = doc.select(".request_level_ms");

            carLoadList.clear();
            for (Element element : elements) {
                id = element.select(".request_level_ms").attr("request_id");
                fromTo = element.select(".request_distance");
                date = element.select(".multi_date");
                price = element.select(".m_comment pr_10");
                type = element.select(".truck");
                weight = element.select(".weight");
                size = element.select(".cube");


                List<Address> fromAddresses = getFromToAddresses(fromTo.text(), true);
                List<Address> toAddresses = getFromToAddresses(fromTo.text(), false);
                if (fromAddresses.size() > 0 || toAddresses.size() > 0) {
                    CarLoad carLoad = new CarLoad( id, fromAddresses, toAddresses, date.text(),
                            type.text(), getDouble(weight.text()), getDouble(size.text()) != null ? new Size(getDouble(size.text())) : null, price.text());


                    carLoadList.add(carLoad);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        catch (WrongInputException e) {
            e.printStackTrace();
        }
        return carLoadList;*/
    }


//
//    private List<Address> getFromToAddresses(String input, boolean isFrom) {
//        List<Address> addresses = new ArrayList<>();
//        if (!"".equals(input)) {
//            String[] fromToStr = input.split(String.valueOf(Character.toChars(8212)));
//            if(fromToStr[isFrom ? 0 : 1] != null) {
//                String[] stringDividedByCommas = fromToStr[isFrom ? 0 : 1].split(",");
//                for (int l = 0; l < stringDividedByCommas.length; l++) {
//                    String[] stringDividedByGaps = stringDividedByCommas[l].split(" ");
//                    int k;
//                    for (k = 0; k < stringDividedByGaps.length; k++) {
//                        if (stringDividedByGaps[k].contains("(") && stringDividedByGaps[k].contains(")")) {
//                            break;
//                        }
//                    }
//                    CountryCode country = CountryCode.valueOf(stringDividedByGaps[k].replaceAll("[\\(\\)]", ""));
//                    String city;
//                    int j = 0;
//                    do {
//                        city = stringDividedByGaps[j++];
//                    }
//                    while (j < k);
//                    addresses.add(new Address(country, city));
//                }
//            }
//        }
//        return addresses;
//    }
//
//    private Double getDouble(String inputStr) throws WrongInputException {
//        Double result = 0d;
//        inputStr.replace(",", ".");
//        Pattern pattern = Pattern.compile("\\d+([\\.]\\d+)?");
//        Matcher matcher = pattern.matcher(inputStr);
//        if (matcher.find()) {
//            result = Double.valueOf(matcher.group());
//        }
//        return result;
//    }

    @Override
    protected void onPostExecute(List<CarLoad> carLoads) {
        Log.v(TAG, "in onPostExecute");
        //carLoadList = carLoads;
        //adapter.notifyDataSetChanged();
    }

    public static void setSearchUrl(String searchUrl) {
        //SEARCH_URL = searchUrl;
    }
}
